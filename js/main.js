import { initBoard } from "./board.js";
import {initBall} from "./ball.js";
import {initPaddle} from "./paddle.js";
import {initBrick} from "./bricks.js";


function startGame(){
    initBoard();
    initBall();
    initPaddle();
    initBrick();
   
}

startGame();
import { Bodies, Composite, engine, world } from './matter.js';

export function initBall(){
     let Ball = Bodies.circle(700,260,20);
    Ball.label = "Ball";  
    Composite.add(engine.world,[Ball]);
};
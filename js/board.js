//import commandes matter js
import { Bodies, Composite, engine } from './matter.js'

export function initBoard(){
    let topBoard = Bodies.rectangle(0,0, 2800,8, { isStatic: true});
    topBoard.label = "topBoard";
    let rightBoard = Bodies.rectangle(1400, 0, 8,1600, { isStatic: true});
    rightBoard.label = "rightBoard";
    let leftBoard = Bodies.rectangle(0, 60,8, 1600, { isStatic: true});
    leftBoard.label = "leftBoard";

    Composite.add(engine.world,[topBoard,rightBoard,leftBoard]);
};


